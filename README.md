# CminusProject3tests

CS4121 Project 3 test "suite". 

Converts .cm inputs to .c files, Compiles with gcc, Runs test inputs in the event that they exist, and generates control outputs. 
From the control outputs, the script then compiles the .cm files to .s files, compiles with gcc, and runs the cmc executables with the same test inputs.
Finally it compares them with the control outputs and determines how many test cases were passed. 

To use with Project3: 
Copy input/ and test.sh to the base CminusProject3 folder
```
chmod +x test.sh
./test.sh
```

To add more test cases:
Inside `input/in/` create a folder with the name of the program such that the .cm filename is the same as the folder like...
`1.if.cm -> 1.if`

Inside the folder, create basic text files starting with `1.in` with the inputs that are to be fed to the program
Adding more test cases such as `2.in, 3.in, etc.` will be run in order and the test case will ONLY pass if all of the cases in the set are correct.


To regenerate control inputs (in the event that the test inputs change):
```
./test.sh control
```

To clean the test files:
```
./test.sh clean
```

For e:
```
./test.sh e
```