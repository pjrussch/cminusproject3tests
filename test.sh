#!/bin/bash

# Clean executables and stuff that we leave after testing
if [ "$1" == "clean" ]; then
    # remove test executables
    rm -rf input/*.s
    rm -rf input/control/*
    rm -rf input/out/*
    rm -rf input/testOut/*
    
    rm -rf input/.controlCreated
    exit 0;
fi

# e.
if [ "$1" = "e" ]; then
    for i in input/*.cm; do
        e=$( echo $i | cut -d'/' -f 2 )
        e=$( echo $e | cut -d'.' -f 1,2 )
        echo $e
    done;
    exit 0;
fi

# Run the control tests with GCC and .CM -> .C
if [ "$1" == "control" ] || [ ! -f "input/.controlCreated" ]; then
    # Delete the existing control values 
    rm -rf input/control/*

    # Run all inputs in the input folder
    for i in input/*.cm; do
        
        echo -e "\e[93m***** Running for input $i *****\e[39m"

        # Setup the output file and executable for the control
        controlOutput=$( echo $i | cut -d'.' -f 1,2 )".c"
        controlOutput="input/control/"$( echo $controlOutput | cut -d'/' -f 2)
        controlExe=$( echo $controlOutput | cut -d'.' -f 1,2 )

        echo $controlOutput

        # From Xhenlin's Email. Converts .cm to .c
        sed -E \
        -e '1s/^/#include <stdio.h>\n\n/' \
        -e 's/read\(/scanf("%d", \&/' \
        -e 's/write\("(.*)"\)/printf("%s\\n", "\1")/' \
        -e 's/write\(([a-z].*)\)/printf("%d\\n", \1)/' \
        -e 's/write\(([0-9].*)\)/printf("%d\\n", \1)/' \
        $i > $controlOutput 

        echo "Compiling $i to $controlExe"

        # Compile the converted .cm file
        gcc $controlOutput -o $controlExe
    
        echo ""

        # Get the test directory
        case=$( echo $i | cut -d'/' -f 2 )
        case=$( echo $case | cut -d'.' -f 1,2 )
        
        testDir="input/in/$case"

        # If an input set exists, run the provided test cases
        if [ -d "$testDir" ]; then 
            echo "Test set exists for $case"

            # Create the output directory
            mkdir -p "input/out/$case"

            for j in $testDir/*.in; do
                echo "    Running $controlExe with < $j"
                
                # Getting the name of the test case
                num=$( echo $j | cut -d'/' -f 4 )
                num=$( echo $num | cut -d'.' -f 1 )".out"
                
                # Run the test
                ./$controlExe < $j > "input/out/$case/$num"
            done;
        else

            mkdir -p "input/out/$case"
                    
            # Run the test
            echo "    Running $controlExe"
            ./$controlExe > "input/out/$case/1.out"

        fi
        
        echo ""

    done;

    echo "Control Set" > input/.controlCreated

    if [ "$2" == "stop" ]; then
        exit 0
    fi
fi

# Now to run the tests using cmc
passed=$(expr 0)
logFile="log.txt"
echo "Running Tests" > $logFile
for i in input/*.cm; do
    
    echo -e "\e[93m***** Running for input $i *****\e[39m"

    ./cmc "$i"

    # Get the case we are working with
    case=$( echo $i | cut -d'/' -f 2 )
    case=$( echo $case | cut -d'.' -f 1,2 )

    sFile=$( echo $i | cut -d'.' -f 1,2 )".s"
    exeName="input/testOut/$case.test" 

    echo "Compiling with $sFile -> $exeName"

    # Compile the .s to an executable
    gcc "$sFile" -o "$exeName"
    
    # Test the current excutable with
    testDir="input/in/$case"

    # If an input set exists, run the provided test cases
    if [ -d "$testDir" ]; then 
        echo "Test set exists for $case"

        # Create the output directory
        mkdir -p "input/testOut/$case"
        
        setPassed=$(expr 0)

        for j in $testDir/*.in; do
            echo "    Running $exeName with < $j"
            
            # Getting the name of the test case
            num=$( echo $j | cut -d'/' -f 4 )
            num=$( echo $num | cut -d'.' -f 1 )".out"
            
            # setup the output file
            outFile="input/testOut/$case/$num"
            answerFile="input/out/$case/$num"
            
            # Run the test
            ./$exeName < $j > $outFile
            
            # Check that the test case has been passed
            result=$(diff $answerFile $outFile)
            if [ ! -z "$result" ]; then
                setPassed="$j"
                break
            else
                echo "    Passed $j"
            fi
        done;

        # Check if the entire test set was passed
        if [ "$setPassed" == "0" ]; then
            echo "Pass"
            passed=$(($passed + 1))
        else
            echo "Failed on $setPassed"
            echo $result
        fi
    else

        mkdir -p "input/testOut/$case"

        outFile="input/testOut/$case/$num"
        answerFile="input/out/$case/1.out"

        # Run the test
        echo "    Running $controlExe"
        ./$exeName > $outFile

        # Check the result against the answer
        result=$(diff $answerFile $outFile)
        if [ ! -z "$result" ]; then
            echo "Fail"
            echo $result
        else
            echo "Pass"
            passed=$(($passed + 1))
        fi
    fi

    echo ""
done;

echo "Passed test cases: $passed"
